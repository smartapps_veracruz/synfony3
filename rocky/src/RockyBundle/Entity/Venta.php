<?php

namespace RockyBundle\Entity;

/**
 * Venta
 */
class Venta
{
    /**
     * @var string
     */
    private $idVenta;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var string
     */
    private $monto;

    /**
     * @var integer
     */
    private $semana;

    /**
     * @var \RockyBundle\Entity\Cliente
     */
    private $idCliente;

    /**
     * @var \RockyBundle\Entity\Vehiculo
     */
    private $placa;

    /**
     * @var \RockyBundle\Entity\ProductoVersion
     */
    private $idVersion;


    /**
     * Get idVenta
     *
     * @return string
     */
    public function getIdVenta()
    {
        return $this->idVenta;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Venta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set monto
     *
     * @param string $monto
     *
     * @return Venta
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return string
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set semana
     *
     * @param integer $semana
     *
     * @return Venta
     */
    public function setSemana($semana)
    {
        $this->semana = $semana;

        return $this;
    }

    /**
     * Get semana
     *
     * @return integer
     */
    public function getSemana()
    {
        return $this->semana;
    }

    /**
     * Set idCliente
     *
     * @param \RockyBundle\Entity\Cliente $idCliente
     *
     * @return Venta
     */
    public function setIdCliente(\RockyBundle\Entity\Cliente $idCliente = null)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return \RockyBundle\Entity\Cliente
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set placa
     *
     * @param \RockyBundle\Entity\Vehiculo $placa
     *
     * @return Venta
     */
    public function setPlaca(\RockyBundle\Entity\Vehiculo $placa = null)
    {
        $this->placa = $placa;

        return $this;
    }

    /**
     * Get placa
     *
     * @return \RockyBundle\Entity\Vehiculo
     */
    public function getPlaca()
    {
        return $this->placa;
    }

    /**
     * Set idVersion
     *
     * @param \RockyBundle\Entity\ProductoVersion $idVersion
     *
     * @return Venta
     */
    public function setIdVersion(\RockyBundle\Entity\ProductoVersion $idVersion = null)
    {
        $this->idVersion = $idVersion;

        return $this;
    }

    /**
     * Get idVersion
     *
     * @return \RockyBundle\Entity\ProductoVersion
     */
    public function getIdVersion()
    {
        return $this->idVersion;
    }
}

