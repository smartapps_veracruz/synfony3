<?php

namespace RockyBundle\Entity;

/**
 * UsuariosVenta
 */
class UsuariosVenta
{
    /**
     * @var integer
     */
    private $idUsuarioVenta;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $apellidom;

    /**
     * @var string
     */
    private $apellidop;

    /**
     * @var string
     */
    private $password;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get idUsuarioVenta
     *
     * @return integer
     */
    public function getIdUsuarioVenta()
    {
        return $this->idUsuarioVenta;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return UsuariosVenta
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidom
     *
     * @param string $apellidom
     *
     * @return UsuariosVenta
     */
    public function setApellidom($apellidom)
    {
        $this->apellidom = $apellidom;

        return $this;
    }

    /**
     * Get apellidom
     *
     * @return string
     */
    public function getApellidom()
    {
        return $this->apellidom;
    }

    /**
     * Set apellidop
     *
     * @param string $apellidop
     *
     * @return UsuariosVenta
     */
    public function setApellidop($apellidop)
    {
        $this->apellidop = $apellidop;

        return $this;
    }

    /**
     * Get apellidop
     *
     * @return string
     */
    public function getApellidop()
    {
        return $this->apellidop;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return UsuariosVenta
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return UsuariosVenta
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}

