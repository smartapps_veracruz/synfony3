<?php

namespace RockyBundle\Entity;

/**
 * Vehiculo
 */
class Vehiculo
{
    /**
     * @var string
     */
    private $placa;

    /**
     * @var integer
     */
    private $capacidad;

    /**
     * @var \RockyBundle\Entity\Cliente
     */
    private $idCliente;

    /**
     * @var \RockyBundle\Entity\TipoVehiculo
     */
    private $tipo;


    /**
     * Get placa
     *
     * @return string
     */
    public function getPlaca()
    {
        return $this->placa;
    }

    /**
     * Set capacidad
     *
     * @param integer $capacidad
     *
     * @return Vehiculo
     */
    public function setCapacidad($capacidad)
    {
        $this->capacidad = $capacidad;

        return $this;
    }

    /**
     * Get capacidad
     *
     * @return integer
     */
    public function getCapacidad()
    {
        return $this->capacidad;
    }

    /**
     * Set idCliente
     *
     * @param \RockyBundle\Entity\Cliente $idCliente
     *
     * @return Vehiculo
     */
    public function setIdCliente(\RockyBundle\Entity\Cliente $idCliente = null)
    {
        $this->idCliente = $idCliente;

        return $this;
    }

    /**
     * Get idCliente
     *
     * @return \RockyBundle\Entity\Cliente
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Set tipo
     *
     * @param \RockyBundle\Entity\TipoVehiculo $tipo
     *
     * @return Vehiculo
     */
    public function setTipo(\RockyBundle\Entity\TipoVehiculo $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \RockyBundle\Entity\TipoVehiculo
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}

