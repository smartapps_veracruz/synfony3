<?php

namespace RockyBundle\Entity;

/**
 * TipoVehiculo
 */
class TipoVehiculo
{
    /**
     * @var integer
     */
    private $idTipoVehiculo;

    /**
     * @var string
     */
    private $tipoVehiculo;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get idTipoVehiculo
     *
     * @return integer
     */
    public function getIdTipoVehiculo()
    {
        return $this->idTipoVehiculo;
    }

    /**
     * Set tipoVehiculo
     *
     * @param string $tipoVehiculo
     *
     * @return TipoVehiculo
     */
    public function setTipoVehiculo($tipoVehiculo)
    {
        $this->tipoVehiculo = $tipoVehiculo;

        return $this;
    }

    /**
     * Get tipoVehiculo
     *
     * @return string
     */
    public function getTipoVehiculo()
    {
        return $this->tipoVehiculo;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return TipoVehiculo
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}

