<?php

namespace RockyBundle\Entity;

/**
 * TipoUsuarios
 */
class TipoUsuarios
{
    /**
     * @var integer
     */
    private $idTipoUsuario;

    /**
     * @var string
     */
    private $tipoUsuario;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get idTipoUsuario
     *
     * @return integer
     */
    public function getIdTipoUsuario()
    {
        return $this->idTipoUsuario;
    }

    /**
     * Set tipoUsuario
     *
     * @param string $tipoUsuario
     *
     * @return TipoUsuarios
     */
    public function setTipoUsuario($tipoUsuario)
    {
        $this->tipoUsuario = $tipoUsuario;

        return $this;
    }

    /**
     * Get tipoUsuario
     *
     * @return string
     */
    public function getTipoUsuario()
    {
        return $this->tipoUsuario;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return TipoUsuarios
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}

