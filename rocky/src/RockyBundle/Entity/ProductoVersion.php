<?php

namespace RockyBundle\Entity;

/**
 * ProductoVersion
 */
class ProductoVersion
{
    /**
     * @var integer
     */
    private $idVersion;

    /**
     * @var float
     */
    private $precio;

    /**
     * @var \RockyBundle\Entity\Producto
     */
    private $idProducto;


    /**
     * Get idVersion
     *
     * @return integer
     */
    public function getIdVersion()
    {
        return $this->idVersion;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return ProductoVersion
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set idProducto
     *
     * @param \RockyBundle\Entity\Producto $idProducto
     *
     * @return ProductoVersion
     */
    public function setIdProducto(\RockyBundle\Entity\Producto $idProducto = null)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get idProducto
     *
     * @return \RockyBundle\Entity\Producto
     */
    public function getIdProducto()
    {
        return $this->idProducto;
    }
}

