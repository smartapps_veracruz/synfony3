<?php

namespace RockyBundle\Entity;

/**
 * Sesiones
 */
class Sesiones
{
    /**
     * @var integer
     */
    private $idSesiones;

    /**
     * @var string
     */
    private $jwt;

    /**
     * @var \DateTime
     */
    private $fechaSesion;

    /**
     * @var \RockyBundle\Entity\Usuarios
     */
    private $idUsuario;


    /**
     * Get idSesiones
     *
     * @return integer
     */
    public function getIdSesiones()
    {
        return $this->idSesiones;
    }

    /**
     * Set jwt
     *
     * @param string $jwt
     *
     * @return Sesiones
     */
    public function setJwt($jwt)
    {
        $this->jwt = $jwt;

        return $this;
    }

    /**
     * Get jwt
     *
     * @return string
     */
    public function getJwt()
    {
        return $this->jwt;
    }

    /**
     * Set fechaSesion
     *
     * @param \DateTime $fechaSesion
     *
     * @return Sesiones
     */
    public function setFechaSesion($fechaSesion)
    {
        $this->fechaSesion = $fechaSesion;

        return $this;
    }

    /**
     * Get fechaSesion
     *
     * @return \DateTime
     */
    public function getFechaSesion()
    {
        return $this->fechaSesion;
    }

    /**
     * Set idUsuario
     *
     * @param \RockyBundle\Entity\Usuarios $idUsuario
     *
     * @return Sesiones
     */
    public function setIdUsuario(\RockyBundle\Entity\Usuarios $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \RockyBundle\Entity\Usuarios
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}

