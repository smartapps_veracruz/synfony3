<?php 
	namespace RockyBundle\Services;
	use Firebase\JWT\JWT;
	/**
	 * 
	 */
	class JWTAuth
	{
		public 	$Manager;
		//Asigno una llave de encriptado
		public 	$Key;


		public function __construct($Manager)
		{
			$this->Key ="Inversa";
			$this->Manager = $Manager;
		}

	    public function CredentialsAuth($Email, $Password)
	    {
	    	//Hago la consulta en la base de datos para que guardar la coincidencia en User
	    	$User= $this->Manager->getRepository("RockyBundle:Usuarios")->findOneBy(
	    		array(
	    			"email"=> $Email,
	    			"password"=>$Password,
	    			"status" =>"1"
	    		)
	    	);

	    	//Pregunto si la variable User es un objeto (ya que en caso contrario seria un error)
	    	if(is_object($User))
	    	{
	    		$Token = array(
	    			"ID_Usuario" => $User->getIdUsuario(),
	    			"Email" => $User->getEmail(),
	    			"Tipo" => $User->getTipo(),
	    			"Nombre" => $User->getNombre(),
	    			"ApellidoM" => $User->getApellidom(),
	    			"ApellidoP" => $User->getApellidop(),

	    			"Date" => time(),
	    			"ExpTime" => time() + (30*60)
	    		);

	    		//Encripto los datos en JWT
	    		$JWT= JWT::encode($Token,$this->Key, 'HS256');

	    		//Desencripto los datos que se encuentran en JWT para acceder a ellos como array
	    		$Decoded = JWT::decode($JWT, $this->Key, array('HS256'));

	    		//Si en los parametros encuentro true como valor de la variable Hash
	    		/*if($Hash != null)
	    		{
	    			//Si existe Hash, regreso el token que se crea en combinacion con los datos del usuario recogido
	    			return array(
	    			"Response" => "Success",
	    			"Token" => $JWT);
	    		}
	    		else
	    		{
	    			//Si no existe Hash, regreso los datos del usuario de manera integra
	    			return $Decoded;
	    		}*/
	    		return array(
	    			"Response" => "Success",
	    			"Token" => $JWT);
	    	}
	    	else
	    	{
	    		//No se encontraron usuarios con el email y password
	    		//Regreso un array con el error
	    		$SignedUp=false;
	    		return array(
	    			"Response" => "Error",
	    			"Description" => "No se ha encontrado algun usuario con las credenciales ingresadas."
	    		);
	    	}
	    }

	    public function VerifyToken($JWT , $Identity = false)
	    {
	    	$Auth = false;

	    	try
	    	{
	    		$Decoded = JWT::decode($JWT, $this->Key, array('HS256'));
	    	}
	    	catch(\UnexpectedValueException $Exception)
	    	{
				$Auth = false;
	    	}
	    	catch(\DomainException $Exception)
	    	{
	    		$Auth = false;
	    	}

	    	if(isset($Decoded->ID_Usuario))
	    	{
	    		$Auth = true;
	    	}
	    	else
	    	{
	    		$Auth = false;
	    	}


	    	if($Identity == true)
	    	{
	    		return $Decoded;
	    	}
	    	else
	    	{
	    		return $Auth;
	    	}
	    }


	}

	
?>