<?php 
	namespace RockyBundle\Services;
	/**
	 * 
	 */
	class Helpers
	{
		public $JWTAuth;

		public function __construct($JWTAuth)
		{
			$this->JWTAuth = $JWTAuth;
		}

		public function CheckToken($JWT, $Identity = false)
		{
			$Auth = false;
			if($JWT != null)
			{
				if($Identity == false)
				{
					$Check_Token = $this->JWTAuth->VerifyToken($JWT);
					if($Check_Token == true)
					{
						$Auth = true;
					}
				}
				else
				{
					$Check_Token = $this->JWTAuth->VerifyToken($JWT, true);

					if(is_object($Check_Token))
					{

						$Auth = $Check_Token;
					}
				}
				return $Auth;
			}
			else
			{
				return $this->JSON(
						array("Status" => "Error",
							  "Description" => "No se ha recibido el objeto JSON de manera correcta."
						)
					);
			}
		}

	    public function JSON($Object)
	    {
	    	$Normalizers = array(new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer());
	    	$Encoders = array("json"=> new \Symfony\Component\Serializer\Encoder\JsonEncoder());

	    	$Serializer = new \Symfony\Component\Serializer\Serializer($Normalizers,$Encoders);
	    	$JSON = $Serializer->serialize($Object, 'json');

	    	$Response = new \Symfony\Component\HttpFoundation\Response();
	    	$Response->setContent($JSON);
	    	$Response->headers->set("Content-Type","application/json");
	    	return $Response;
	    }

	}

	
?>