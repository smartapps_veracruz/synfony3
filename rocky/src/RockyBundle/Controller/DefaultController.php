<?php
//Para renderizar vistas
namespace RockyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $Request)
    {
        return $this->render('RockyBundle:Credentials:login.html.twig');
    }

    public function ventaAction(Request $Request)
    {
        return $this->render('RockyBundle:Venta:VentaForm.html.twig');
    }
    
}
