<?php 
	namespace RockyBundle\Controller;

	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\Validator\Constraints as Assert;

	class LoginController extends Controller
	{
		//Login:Vacios:GenerarTokenJWT:Hora
		//CreateUser:
		//RecieveProducts:if isset(token) return *

		public function authAction(Request $Request)
		{
			//Importo mis servicios
			$Helper = $this->get("rocky.helpers");
			$JWTAuth = $this->get("rocky.jwtauth");

			//Recibo el objeto JSON de la vista usando el Request del parametro
			$CredentialsJSON = $Request->getContent();

			//Pregunto si el JSON que recibí no se encuentra vacío
			if($CredentialsJSON!= null)
			{
				//Las credenciales que obtuve desde la vista (formato JSON) las convierto a objeto de PHP
				$Parameters =json_decode($CredentialsJSON, true);

				//Pregunto por el valor individual de cada uno de los espacios del objeto
				$Email = (isset($Parameters["Email"])) ? $Parameters["Email"] : null;
				$Password = (isset($Parameters["Password"])) ? $Parameters["Password"] : null;
				//$Hash = (isset($Parameters["Hash"])) ? $Parameters["Hash"] : null;

				//Creo un objeto del validador de Symfony para garantizar la recepción de un Email
				$EmailConstraint = new Assert\Email();
				//Le asigno un mensaje de error en caso de que lo necesite
				$EmailConstraint->message = "The email provided is incorrect.";
				//Pregunto (usando el validador) si el Email que recibí cumple con las propiedades que el validador propone
				$Validate_Email = $this->get("validator")->validate($Email, $EmailConstraint);

				//Validate_Email regresa un mensaje si algo anda mal, en caso contrario no regresa nada

				//Si el email es correcto y el password no esta vacio
				if(count($Validate_Email) == 0 && $Password != null)
				{
					/*if($Hash == null)
					{
						$Response = $JWTAuth->CredentialsAuth($Email, md5($Password));
					}
					else
					{
						$Response = $JWTAuth->CredentialsAuth($Email, md5($Password), true);
					}*/
					$Response = $JWTAuth->CredentialsAuth($Email, md5($Password));
					return $Helper->JSON($Response);
				}
				else
				{
					return $Helper->JSON(
						array("Status" => "Error",
							  "Description" => "Alguno de las credenciales ingresadas es incorrecta."
						)
					);
				}
			}	
			else
			{
				return $Helper->JSON(
						array("Status" => "Error",
							  "Description" => "No se ha recibido el objeto JSON de manera correcta."
						)
					);
			}
		}
		
		public function isloggedAction(Request $Request)
		{
			$Helper = $this->get("rocky.helpers");

			$HashJSON = $Request->getContent();
			if($HashJSON != null)
			{
				$Hash =json_decode($HashJSON, true);
				//Si recibo un indice en el JSON llamado ID y ademas tiene un valor igual a true
				/*if($Hash["ID"]==true)
				{
					//Pido el objeto con toda la informacion decodificada del TOKEN
					$Check = $Helper->CheckToken($Hash["JWT"],true);
				}
				else
				{
					//Caso contrario solo requiero saber si el token es correcto (Devuelve booleano)
					$Check = $Helper->CheckToken($Hash["JWT"]);
				}
				*/
				
				$Check = $Helper->CheckToken($Hash["JWT"]);
				return $Helper->JSON($Check);
			}
		}


		public function logoutAction(Request $Request)
		{

		}

		public function refreshTokenAction(Request $Request)
		{

		}

	}
?>